from timeit import default_timer as timer
import matplotlib.pyplot as plt
import numpy as np
from sklearn import svm
from sklearn.metrics import r2_score, mean_squared_error

from Plot import plot_train_set, plot_regression
from kernels import rbf_kernel
from svr.LSSVR import LSSVR

exponent = np.exp(1)


def f(x):
    return 1.5 * x - 3 * np.power(exponent, -(x - 4.5) ** 2 / 0.15)


def generate_dataset(train_size, test_size, use_noise=False):
    lb = 2.
    hb = 10.
    x_train = np.sort(np.random.uniform(low=lb, high=hb, size=(train_size,))).reshape(-1, 1)
    x_test = np.atleast_2d(np.linspace(lb, hb, test_size)).T

    y_train = f(x_train).ravel()
    y_test = f(x_test).ravel()

    if use_noise:
        gaussian_noise = np.random.normal(0, 1, train_size)
        y_train = y_train + gaussian_noise
    print("Размер обучающей выборки равен: {}".format(train_size))
    print("Размер тестовой выборки равен: {}".format(test_size))
    return x_train, y_train, x_test, y_test


def run_svr(name, model, x_train, y_train, x_test, y_test, verbose=False):
    if verbose:
        print("\nЗапуск {} метода".format(name))

    start = timer()
    model.fit(x_train, y_train)
    end = timer()

    if verbose:
        print("Время обучения:\t{} sec".format(end - start))

    start = timer()
    y_predict = model.predict(x_test)
    end = timer()

    if verbose:
        print("Время предсказания:\t{} sec".format(end - start))
    return name, y_predict, mean_squared_error(y_test, y_predict), r2_score(y_test, y_predict)


def exec_svr_optimization():
    x_train, y_train, x_test, y_test = generate_dataset(10000, 1000)

    results = {}

    for i in range(-5, 5):
        for j in range(-5, 5):
            C = 10 ** i
            Gamma = 10 ** j

            svr_model = svm.SVR(kernel='rbf', C=C, gamma=Gamma)
            svr_model.fit(x_train, y_train)
            svr_predict = svr_model.predict(x_test)

            lssvr_model = LSSVR(kernel=rbf_kernel, gamma=Gamma, factor=C)
            lssvr_model.fit(x_train, y_train)
            lssvr_predict = lssvr_model.predict(x_test)

            svr_r2 = r2_score(y_test, svr_predict)
            lssvr_r2 = r2_score(y_test, lssvr_predict)

            svr_mse = mean_squared_error(y_test, svr_predict)
            lssvr_mse = mean_squared_error(y_test, lssvr_predict)

            results[(C, Gamma)] = (lssvr_mse, svr_mse)

            print("C = {}, Gamma = {}".format(C, Gamma))

            print('R2: {} vs {}'.format(lssvr_r2, svr_r2))
            print('MSE: {} vs {}'.format(lssvr_mse, svr_mse))
    print(min(results, key=results.get))
    print(results[min(results, key=results.get)])


def exec_regression(C, Gamma, train_size, test_size, noise, verbose, plot):
    x_train, y_train, x_test, y_test = generate_dataset(train_size, test_size, noise)

    name_lssvr, y_predict_lssvr, mse_lssvr, r2_lssvr = run_svr("LS-SVR",
                                                               LSSVR(kernel=rbf_kernel, gamma=Gamma, factor=C), x_train,
                                                               y_train, x_test,
                                                               y_test, verbose)
    name_svr, y_predict_svr, mse_svr, r2_svr = run_svr("SVR", svm.SVR(kernel='rbf', C=C, gamma=Gamma),
                                                       x_train, y_train, x_test,
                                                       y_test, verbose)

    print("\nC = {}, Gamma = {}\n".format(C, Gamma))
    print("R2:\n{}\t= {}\n{}\t= {}\n".format(name_lssvr, r2_lssvr, name_svr, r2_svr))
    print("MSE:\n{}\t= {:.5E}\n{}\t= {:.5E}".format(name_lssvr, mse_lssvr, name_svr, mse_svr))

    if plot:
        plot_train_set(2, 10, f, x_train, y_train, title="Обучающее множество (размер: {})".format(len(x_train)))
        plot_regression(2, 10, f, x_test, y_predict_lssvr, y_predict_svr,
                        title="Тестовое множество (размер: {})".format(len(x_test)))


def estimate_params():
    train_size = 1000
    test_size = 1000
    noise = True
    x_train, y_train, x_test, y_test = generate_dataset(train_size, test_size, noise)
    r2_scores = {'ls-svr': [], 'svr': []}
    mse_scores = {'ls-svr': [], 'svr': []}
    learning_time = {'ls-svr': [], 'svr': []}
    prediction_time = {'ls-svr': [], 'svr': []}
    values = [10 ** i for i in range(-3, 8)]

    for i in range(0, len(values)):
            Gamma = values[i]
            C = 1

            svr_model = svm.SVR(kernel='rbf', C=C, gamma=Gamma)
            start = timer()
            svr_model.fit(x_train, y_train)
            end = timer()
            learning_time['svr'].append(end-start)
            start = timer()
            svr_predict = svr_model.predict(x_test)
            end = timer()
            prediction_time['svr'].append(end-start)

            lssvr_model = LSSVR(kernel=rbf_kernel, gamma=Gamma, factor=C)
            start = timer()
            lssvr_model.fit(x_train, y_train)
            end = timer()
            learning_time['ls-svr'].append(end - start)
            start = timer()
            lssvr_predict = lssvr_model.predict(x_test)
            end = timer()
            prediction_time['ls-svr'].append(end - start)

            svr_r2 = r2_score(y_test, svr_predict)
            lssvr_r2 = r2_score(y_test, lssvr_predict)
            r2_scores['svr'].append(svr_r2)
            r2_scores['ls-svr'].append(lssvr_r2)

            svr_mse = mean_squared_error(y_test, svr_predict)
            lssvr_mse = mean_squared_error(y_test, lssvr_predict)
            mse_scores['svr'].append(svr_mse)
            mse_scores['ls-svr'].append(lssvr_mse)

            print("C = {}, Gamma = {}".format(C, Gamma))
            print('R2: {} vs {}'.format(lssvr_r2, svr_r2))
            print('MSE: {} vs {}'.format(lssvr_mse, svr_mse))

    params = {'axes.labelsize': 16,
              'axes.titlesize': 16,
              'legend.fontsize': 16,
              'figure.figsize': (16, 9)}
    plt.rcParams.update(params)

    plt.figure()
    plt.plot(values, learning_time['svr'], 'blue', lw=2, label='SVR')
    plt.plot(values, learning_time['ls-svr'], 'red', lw=2, label='LS-SVR')
    plt.title("Зависимость скорости обучения при росте $Gamma$")
    plt.xlabel('Значения $Gamma$')
    plt.ylabel('Время, сек')
    plt.legend()
    plt.show()

    plt.figure()
    plt.plot(values, prediction_time['svr'], 'blue', lw=2, label='SVR')
    plt.plot(values, prediction_time['ls-svr'], 'red', lw=2, label='LS-SVR')
    plt.title("Зависимость скорости предсказания при росте $Gamma$")
    plt.xlabel('Значения $Gamma$')
    plt.ylabel('Время, сек')
    plt.legend()
    plt.show()
