import argparse

import numpy as np

from svr.main import exec_regression
from svm.main import exec_classification
from my_types import restricted_float, restricted_int


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-v', '--verbose', action='store_true', help='Включить расширенный лог')
    parser.add_argument('-p', '--plot', action='store_true', help='Визуализировать результаты')
    parser.add_argument('--seed', type=restricted_int, help='Задает начальные условия для генератора случайных чисел.',
                        default=3)

    subparser = parser.add_subparsers(dest='command')

    parser_svm = subparser.add_parser('svm', help="Используется для процесса классификации (Support Vector Machine)")
    parser_svm.add_argument('-g', '--gamma', type=restricted_float, help='Коэффициент ядра', default=1.2)
    parser_svm.add_argument('-c', '--factor', type=restricted_float, help='Коэффициент регуляризации', default=0.01)
    parser_svm.add_argument('-t', '--test', required=True, choices=[1, 2], type=int, help='Номер тестовой выборки')

    parser_svr = subparser.add_parser('svr', help="Используется для процесса регрессии (Support Vector Regression)")
    parser_svr.add_argument('-g', '--gamma', required=True, type=restricted_float, help='Коэффициент ядра')
    parser_svr.add_argument('-c', '--factor', required=True, type=restricted_float, help='Коэффициент регуляризации')
    parser_svr.add_argument('-tns', '--train-size', type=restricted_int, help='Размер обучающей выборки', default=1000)
    parser_svr.add_argument('-tts', '--test-size', type=restricted_int, help='Размер тестовой выборки', default=1000)
    parser_svr.add_argument('--noise', action='store_true', help='Добавить в обучающую выборку Гауссовский шум')

    args = parser.parse_args()

    np.random.seed(args.seed)

    if args.command == 'svr':
        exec_regression(args.factor, args.gamma, args.train_size, args.test_size, args.noise, args.verbose, args.plot)
    elif args.command == 'svm':
        exec_classification(args.factor, args.gamma, args.test, args.verbose, args.plot)
    else:
        print("Требуется указать идентификатор процесса, проверьте --help.")
