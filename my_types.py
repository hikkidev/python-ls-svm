import argparse


def restricted_float(x):
    try:
        x = float(x)
        if x <= 0.0:
            raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r не литерал с плавающей точкой" % (x,))
    return x


def restricted_int(x):
    try:
        x = int(x)
        if x <= 0:
            raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r не целочисленный литерал" % (x,))
    return x