import matplotlib.pyplot as plt
import numpy as np

from sklearn.decomposition import PCA

params = {'axes.labelsize': 16,
          'axes.titlesize': 16,
          'legend.fontsize': 16,
          'figure.figsize': (16, 9)}
plt.rcParams.update(params)


def plot_regression(lb, hb, fun, x_test, y_ls_svr, y_svr,  title):
    plt.figure()
    x = np.atleast_2d(np.linspace(lb, hb, 500)).ravel()
    y = fun(x).ravel()
    plt.plot(x, y, 'cyan', lw=2, label='Искомая функция', alpha=0.8)
    plt.plot(x_test, y_svr, 'blue', lw=2, label='SVR')
    plt.plot(x_test, y_ls_svr, 'red', lw=2, label='LS-SVR')

    plt.title(title)
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.legend()
    plt.show()


def plot_train_set(lb, hb, fun, x, y_hat, title=None):
    figure = plt.figure()
    plt.scatter(x, y_hat,
                marker='o',
                c='blue',
                alpha=0.3,
                label="Наблюдения")
    x = np.atleast_2d(np.linspace(lb, hb, 500)).ravel()
    y = fun(x).ravel()
    plt.plot(x, y, 'r', lw=2, label='Исходная функция', alpha=0.8)

    plt.title(title)
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.legend()
    figure.set_figwidth(16)
    figure.set_figheight(9)
    plt.show()


class Plot:
    def __init__(self):
        self.cmap = plt.get_cmap('plasma')

    # Визуализировать набор данных 'X' и соответствующие метки 'Y' в 2D с помощью PCA.
    def plot_in_2d(self, X, Y=None, title=None, accuracy=None, legend_labels=None):
        pca = PCA(n_components=2)
        principalComponents = pca.fit_transform(X)
        x1 = principalComponents[:, 0]
        x2 = principalComponents[:, 1]
        classes = []

        Y = np.array(Y).astype(int)

        colors = [self.cmap(i) for i in np.linspace(0, 1, len(np.unique(Y)))]

        for i, l in enumerate(np.unique(Y)):
            _x1 = x1[Y == l]
            _x2 = x2[Y == l]
            _y = Y[Y == l]
            classes.append(plt.scatter(_x1, _x2, color=colors[i]))

        if legend_labels is not None:
            plt.legend(classes, legend_labels, loc=1)

        if title:
            if accuracy:
                plt.suptitle(title)
                plt.title("Точность: %.1f%%" % (100 * accuracy), fontsize=10)
            else:
                plt.title(title)

        plt.xlabel('Главная компонента 1')
        plt.ylabel('Главная компонента 2')

        plt.show()
