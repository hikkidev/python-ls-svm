import numpy as np
from scipy.spatial.distance import cdist
from sklearn.metrics.pairwise import euclidean_distances


def rbf_kernel(gamma, **kwargs):
    def fun(x, y):
        if x.ndim == y.ndim and x.ndim == 2:  # x, y - матрицы
            return np.exp(-gamma * cdist(x, y) ** 2)

        else:  # x, y - векторы или x вектор, а y - матрица
            return np.exp(-gamma * euclidean_distances(x, y))

    return fun


def polynomial_kernel(power=1, a=0, **kwargs):
    def fun(x, y):
        return (np.dot(x, y.T) + a) ** power

    return fun
