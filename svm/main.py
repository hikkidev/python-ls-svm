from timeit import default_timer as timer

import numpy as np
import pandas as pd
from sklearn import preprocessing, svm

from Plot import Plot
from kernels import rbf_kernel
from svm.LSSVM import LSSVM


def calc_accuracy(v_true, v_predict):
    return np.sum(v_true == v_predict, axis=0) / len(v_true)


def import_dataset(filename):
    train_data = pd.read_csv("./data/{}.txt".format(filename), sep=",", low_memory=False)
    train_data = train_data.astype(dtype={"Occupancy": "int32",
                                          "Light": "int32",
                                          "Temperature": "float32",
                                          "CO2": "float32",
                                          "Humidity": "float32",
                                          "HumidityRatio": "float64"
                                          })
    y = train_data['Occupancy'].values
    y[y == 0] = -1  # Преобразуем выборку к виду -1\+1
    x = preprocessing.normalize(train_data.drop(columns=['date', 'Occupancy']).to_numpy())
    print("Размер выборки '{}' равен: {}".format(filename, len(x)))

    return x, y


def run_svm(name, model, x_train, y_train, x_test, y_test, verbose=False):
    if verbose:
        print("\nЗапуск {} метода".format(name))

    start = timer()
    model.fit(x_train, y_train)
    end = timer()

    if verbose:
        print("Время обучения:\t{} sec".format(end - start))

    start = timer()
    y_predict = model.predict(x_test)
    end = timer()

    if verbose:
        print("Время классификации:\t{} sec".format(end - start))
    return name, calc_accuracy(y_test, y_predict), y_predict


def exec_svm_optimization():
    x_train, y_train = import_dataset('datatraining')
    x_test, y_test = import_dataset('datatest')
    results = {}

    for i in range(-3, 3):
        for j in range(-3, 3):
            C = 10 ** i
            Gamma = 2 ** j

            print("C = {}, Gamma = {}".format(C, Gamma))
            _, acc_LSSVM, _ = run_svm("LS-SVM", LSSVM(kernel=rbf_kernel, gamma=Gamma, factor=C), x_train, y_train,
                                      x_test,
                                      y_test)
            _, acc_SVM, _ = run_svm("SVM", svm.SVC(kernel='rbf', gamma=Gamma, C=C), x_train, y_train, x_test, y_test)
            results[(C, Gamma)] = (acc_LSSVM, acc_SVM)

            print("Точность: {} vs {}".format(acc_LSSVM, acc_SVM))
    print(results)
    print(max(results, key=results.get))


def exec_classification(C, Gamma, case_num, verbose, plot):
    print("C = {}, Gamma = {}".format(C, Gamma))
    x_train, y_train = import_dataset('datatraining')
    x_test, y_test = import_dataset('datatest{}'.format(case_num))

    name_LSSVM, acc_LSSVM, y_predict_LSSVM = run_svm("LS-SVM", LSSVM(kernel=rbf_kernel, gamma=Gamma, factor=C), x_train,
                                                     y_train, x_test,
                                                     y_test, verbose)
    name_SVM, acc_SVM, y_predict_SVM = run_svm("SVM", svm.SVC(kernel='rbf', gamma=Gamma, C=C), x_train, y_train, x_test,
                                               y_test, verbose)
    print("\nC = {}, Gamma = {}".format(C, Gamma))
    print("Точность:\n{}\t= {}\n{}\t= {}".format(name_LSSVM, acc_LSSVM, name_SVM, acc_SVM))

    if plot:
        Plot().plot_in_2d(x_test, y_predict_LSSVM, title="LS-SVM", accuracy=acc_LSSVM)
        Plot().plot_in_2d(x_test, y_predict_SVM, title="SVM", accuracy=acc_SVM)
