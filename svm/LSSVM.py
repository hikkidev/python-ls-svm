import numpy as np


class LSSVM:
    """Классификатор Least-Squares Support Vector Machine

    Параметры:
    -----------
    kernel: function
        Функция ядра.
    factor: float, default=1.0
        Коэффициент регуляризации. Сила регуляризации обратно пропорциональна factor. Должна быть строго положительной.
    gamma: float, default=1 / n_features
        Коэффициент для ядра.
    """
    def __init__(self, kernel, factor=1.0, gamma=None):
        self.kernel = kernel
        self.factor = factor
        self.gamma = gamma

        self.x = None
        self.y = None

        self.alpha = None
        self.beta = None

    def fit(self, x, y):
        n_samples, n_features = np.shape(x)

        self.x = x
        self.y = y
        if not self.gamma:
            self.gamma = 1 / n_features
        self.kernel = self.kernel(gamma=self.gamma)

        # представление в виде столбца
        y = y[:, np.newaxis]

        A_cross = np.linalg.pinv(np.block([
            [0, y.T],
            [y, self.kernel(x, x) + 1.0 / self.factor * np.identity(len(y))]
        ]))

        B = np.array([0] + [1] * len(y))

        solution = np.dot(A_cross, B)

        self.beta = solution[0]
        self.alpha = solution[1:]

    def predict(self, x):
        return np.sign(np.dot(np.multiply(self.alpha, self.y), self.kernel(self.x, x)) + self.beta)
