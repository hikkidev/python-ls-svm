# Обзор
Репозиторий содержит python реализоцию **Least-Squares Support Vector Machine** для классификации и регрессии.

Для установки зависимостей выполните `pip install -r requirements.txt` в папке с проектом.


## Использование

Подробная информация о глобальных параметрах скрипта
1) Перейти в папку с проектом: `cd python-ls-svm`
2) Выполнить: `python main.py --help`


Инструкция по запуску классификации
1) Перейти в папку с проектом: `cd python-ls-svm`
2) Подробнее о параметрах скрипта: `python main.py svm --help`
3) Выполнить: `python main.py -v -p svm --factor 1e-1 --gamma 1.2 --test 1`


Инструкция по запуску регрессии
1) Перейти в папку с проектом: `cd python-ls-svm`
2) Подробнее о параметрах скрипта: `python main.py svr --help`
3) Выполнить: `python main.py -v -p svr --factor 1e+4 --gamma 2.8 --train-size 1000 --test-size 1000 --noise`


## Реализация

Класс реализующий [LS-SVM](./svm/LSSVM.py)

Класс реализующий [LS-SVR](./svr/LSSVR.py)
